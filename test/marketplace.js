const MarketPlace = artifacts.require("MarketPlace");

contract("MarketPlace", accounts => {
  let marketplace;

  before(async () => {
    marketplace = await MarketPlace.deployed();
  });

  it("should set the initial status to Pending", async () => {
    const status = await marketplace.getStatus();
    assert.equal(status, 0, "Initial status should be Pending");
  });

  it("should allow the owner to change the status to Shipped", async () => {
    await marketplace.ship({ from: accounts[0] });
    const status = await marketplace.getStatus();
    assert.equal(status, 1, "Status should be Shipped");
  });

  it("should allow the owner to change the status to Delivered", async () => {
    await marketplace.deliver({ from: accounts[0] });
    const status = await marketplace.getStatus();
    assert.equal(status, 2, "Status should be Delivered");
  });

  it("should not allow non-owners to call ship()", async () => {
    try {
      await marketplace.ship({ from: accounts[1] });
    } catch (error) {
      assert(error.message.indexOf("Only contract owner can call this function.") >= 0);
    }
  });

  it("should not allow non-owners to call deliver()", async () => {
    try {
      await marketplace.deliver({ from: accounts[1] });
    } catch (error) {
      assert(error.message.indexOf("Only contract owner can call this function.") >= 0);
    }
  });

  it("should not allow the owner to call getStatusForCustomer()", async () => {
    try {
      await marketplace.getStatusForCustomer({ from: accounts[0], value: web3.utils.toWei("1", "ether") });
    } catch (error) {
      assert(error.message.indexOf("Only customer can call this function.") >= 0);
    }
  });

  it("should not allow customers to call getStatusForCustomer() without paying", async () => {
    try {
      await marketplace.getStatusForCustomer({ from: accounts[1] });
    } catch (error) {
      assert(error.message.indexOf("You need to pay at least 1 ether to access this function.") >= 0);
    }
  });

  it("should allow customers to call getStatusForCustomer() after paying", async () => {
    await marketplace.deliver({ from: accounts[0] });
    await marketplace.getStatusForCustomer({ from: accounts[1], value: web3.utils.toWei("1", "ether") });
    const status = await marketplace.getStatus({ from: accounts[0] });
    assert.equal(status, 2, "Status should be Delivered");
  });
});
