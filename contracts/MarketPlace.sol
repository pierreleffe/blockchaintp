// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract MarketPlace {
    enum ShippingStatus { Pending, Shipped, Delivered }
    ShippingStatus private status;

    address private owner;
    address payable public customer;

    event MissionComplete();

    modifier onlyOwner() {
        require(msg.sender == owner, "Only contract owner can call this function.");
        _;
    }

    modifier onlyCustomer() {
        require(msg.sender != owner, "Only customer can call this function.");
        _;
    }

    constructor() {
        status = ShippingStatus.Pending;
        owner = msg.sender;
    }

    function ship() public onlyOwner {
        status = ShippingStatus.Shipped;
    }

    function deliver() public onlyOwner {
        status = ShippingStatus.Delivered;
        emit MissionComplete();
    }

    function getStatus() public view onlyOwner returns (ShippingStatus) {
        return status;
    }

    function getStatusForCustomer() public payable onlyCustomer returns (ShippingStatus) {
        require(msg.value >= 1 ether, "You need to pay at least 1 ether to access this function.");
        return status;
    }
}
