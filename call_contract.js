const fs = require("fs");
const { abi } = JSON.parse(fs.readFileSync("build/contracts/MarketPlace.json"))
async function main() {
const Web3 = require('web3');
const web3 = new Web3('https://rpc-mumbai.maticvigil.com');

const abi = [/* ABI du contrat MarketPlace */];
const address = process.env.ADRESS_WALLET; // Adresse du contrat déployé sur Mumbai

const marketplace = new web3.eth.Contract(abi, address);

const shippingStatus = {
  PENDING: 0,
  SHIPPED: 1,
  DELIVERED: 2,
};

// Création d'une livraison
marketplace.methods.ship().send({ from: process.env.ADRESS_WALLET, gas: '100000' })
  .then((receipt) => {
    console.log('Livraison créée :', receipt);
    // Changement de statut de la livraison
    return marketplace.methods.deliver().send({ from: process.env.ADRESS_WALLET2, gas: '100000' });
  })
  .then((receipt) => {
    console.log('Statut de la livraison changé :', receipt);
    // Récupération du statut de la livraison pour le client
    return marketplace.methods.getStatusForCustomer().send({ from: process.env.ADRESS_WALLET2,
         value: web3.utils.toWei('1', 'ether'), gas: '100000' });
  })
  .then((status) => {
    console.log('Statut de la livraison pour le client :', shippingStatus[status]);
  })
  .catch((error) => {
    console.error(error);
  });
}