require('dotenv').config();
const HDWalletProvider = require('@truffle/hdwallet-provider');

const PRIVATE_KEY = process.env.PRIVATE_KEY;
const ALCHEMY_API_KEY = process.env.ALCHEMY_API_KEY;
const alchemy = `https://polygon-mumbai.g.alchemy.com/v2/${ALCHEMY_API_KEY}`;
module.exports = {
  /**   * $ truffle test --network <network-name>   */
  networks: {
      development: {
        host: "127.0.0.1",
        port: 8546,
        network_id: "*"
      },
    mumbai: {
      provider: () => new HDWalletProvider(PRIVATE_KEY, "https://rpc-mumbai.maticvigil.com"),
      network_id: 80001,
      timeoutBlocks: 200,
      confirmations: 2,
      timeoutBlocks: 200,
      skipDryRun: true

    },
  },
  mocha: {
    // timeout: 100000 
  },
  compilers: {
    solc: {
      version: "0.8.13",
    }
  },
};